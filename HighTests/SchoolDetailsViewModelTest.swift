//
//  SchoolDetailsViewModelTest.swift
//  HighTests
//
//  Created by Consultant on 3/15/23.
//

import XCTest
@testable import High

final class SchoolDetailsViewModelTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testSchoolSatDetails_When_Everything_isCorrect() async{
        let schoolDetailsViewModel = SchoolDetailsViewModel(networkManager: MockManager())
                                                

        XCTAssertNotNil(schoolDetailsViewModel)
        
        let expectation = XCTestExpectation(description: "Fetching School Sat Details ")
        await schoolDetailsViewModel.getSchoolsSatDetails(urlString: "SchoolSatMockData")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5){
            XCTAssertNotNil(schoolDetailsViewModel.schoolDetail)
            let school = schoolDetailsViewModel.schoolDetail
            XCTAssertEqual("423",school?.satMathAvgScore)
            XCTAssertEqual("366",school?.satWritingAvgScore)
            XCTAssertNil(schoolDetailsViewModel.customError)

            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10)
        
    }
    
    func testGetSchoolList_When_URL_isNotGiven() async{
        let schoolDetailsViewModel = SchoolDetailsViewModel(networkManager: MockManager())

        XCTAssertNotNil(schoolDetailsViewModel)

        let expectation = XCTestExpectation(description: "I shouldn't get the data")
        await schoolDetailsViewModel.getSchoolsSatDetails(urlString: "")

        let schoolDetail =  schoolDetailsViewModel.schoolDetail
        let localError =  schoolDetailsViewModel.customError
        DispatchQueue.main.asyncAfter(deadline: .now() + 5){
            
            XCTAssertNil(schoolDetail)
            XCTAssertNotNil(localError)
            XCTAssertEqual(localError,NetworkError.invalidURL)
            expectation.fulfill()
        }
        
    }
    
    func testGetSchoolList_When_URL_isWrong() async{
        let schoolDetailsViewModel = SchoolDetailsViewModel(networkManager: MockManager())
        XCTAssertNotNil(schoolDetailsViewModel)
        let expectation = XCTestExpectation(description: "I shouldn't get the data")

        await schoolDetailsViewModel.getSchoolsSatDetails(urlString: "SchoolSatMockData1234")

        DispatchQueue.main.asyncAfter(deadline: .now() + 5){
            
            XCTAssertNil(schoolDetailsViewModel.schoolDetail)
            XCTAssertNotNil(schoolDetailsViewModel.customError)
            XCTAssertEqual(schoolDetailsViewModel.customError,NetworkError.dataNotFound)
            
            expectation.fulfill()
        }
        
    }
    func testGetSchoolList_When_URL_isCorrect_ButAPIFails() async{
        let schoolDetailsViewModel = SchoolDetailsViewModel(networkManager: MockManager())

        await schoolDetailsViewModel.getSchoolsSatDetails(urlString: "SchoolListDataAPIFailed")
        let expectation = XCTestExpectation(description: "API should fail to give data")
        let schoolsList =  schoolDetailsViewModel.schoolDetail
        DispatchQueue.main.asyncAfter(deadline: .now() + 5){
            XCTAssertNil(schoolDetailsViewModel.schoolDetail)
            XCTAssertEqual(schoolDetailsViewModel.customError,.parsingError)
            expectation.fulfill()
        }

    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
