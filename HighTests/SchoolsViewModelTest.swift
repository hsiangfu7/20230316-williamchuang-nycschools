//
//  SchoolsViewModelTest.swift
//  HighTests
//
//  Created by Consultant on 3/15/23.
//

import XCTest
@testable import High

final class SchoolsViewModelTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testGetSchoolList_When_Everything_isCorrect() async{
        let schoolsViewModel = SchoolsViewModel(networkManager: MockManager())

        XCTAssertNotNil(schoolsViewModel)
        
        let expectation = XCTestExpectation(description: "Fetching Schools list")
        await schoolsViewModel.getSchoolsList(urlString: "SchoolListMockData")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5){
            XCTAssertEqual(schoolsViewModel.schoolsList.count,2)
            let school = schoolsViewModel.schoolsList.first
            XCTAssertEqual("Clinton School Writers & Artists, M.S. 260",school?.school_name)
            XCTAssertEqual("admissions@theclintonschool.net",school?.school_email)
            XCTAssertNil(schoolsViewModel.customError)

            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10)
        
    }
    
    func testGetSchoolList_When_URL_isNotGiven() async{
        let schoolsViewModel = SchoolsViewModel(networkManager: MockManager())

        XCTAssertNotNil(schoolsViewModel)

        let expectation = XCTestExpectation(description: "I shouldn't get the data")
        await schoolsViewModel.getSchoolsList(urlString: "")

        let schoolList =  schoolsViewModel.schoolsList
        let localError =  schoolsViewModel.customError
        DispatchQueue.main.asyncAfter(deadline: .now() + 5){
            
            XCTAssertEqual(schoolList.count,0)
            XCTAssertNotNil(localError)
            XCTAssertEqual(localError,NetworkError.invalidURL)
            expectation.fulfill()
        }
        
    }
    
    func testGetSchoolList_When_URL_isWrong() async{
        let schoolsViewModel = SchoolsViewModel(networkManager: MockManager())
        XCTAssertNotNil(schoolsViewModel)
        let expectation = XCTestExpectation(description: "I shouldn't get the data")

        await schoolsViewModel.getSchoolsList(urlString: "SchoolListMockData12234")

        DispatchQueue.main.asyncAfter(deadline: .now() + 5){
            
            XCTAssertEqual(schoolsViewModel.schoolsList.count,0)
            XCTAssertNotNil(schoolsViewModel.customError)
            XCTAssertEqual(schoolsViewModel.customError,NetworkError.dataNotFound)
            
            expectation.fulfill()
        }
        
    }
    func testGetSchoolList_When_URL_isCorrect_ButAPIFails() async{
        let schoolsViewModel = SchoolsViewModel(networkManager: MockManager())

        await schoolsViewModel.getSchoolsList(urlString: "SchoolListDataAPIFailed")
        let expectation = XCTestExpectation(description: "API should fail to give data")
        let schoolsList =  schoolsViewModel.schoolsList
        DispatchQueue.main.asyncAfter(deadline: .now() + 5){
            XCTAssertEqual(schoolsList.count,0)
            XCTAssertEqual(schoolsViewModel.customError,.parsingError)
            expectation.fulfill()
        }

    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
