//
//  ListCell.swift
//  20230316-FranklinMott-NYCSchools
//
//  Created by Consultant on 3/15/23.
//

import SwiftUI

struct ListCell: View {
    var schoolData: School
    
    var body: some View {
        VStack(alignment: .leading){
            Text("Name: " + (schoolData.school_name ?? "") ).font(.headline)
            Text("Address: " + (schoolData.primary_address_line_1 ?? "") ).font(.title3)
            Text("City: " + (schoolData.city ?? "") ).font(.body)
        }
    }
}

struct ListCell_Previews: PreviewProvider {
    
    @State static var school = School(dbn: "", school_name: "", boro: "", overview_paragraph: "", ell_programs: "", neighborhood: "", building_code: "", location: "", phone_number: "", fax_number: "", school_email: "", website: "", subway: "", extracurricular_activities: "", primary_address_line_1: "", city: "")
    static var previews: some View {
        ListCell(schoolData: school )
    }
}
