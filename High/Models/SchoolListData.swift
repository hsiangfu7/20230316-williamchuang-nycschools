//
//  SchoolListData.swift
//  High
//
//  Created by Consultant on 3/15/23.
//

import Foundation
// MARK: - School
struct School: Codable,Identifiable {
    
    let dbn: String?
    let school_name: String?
    let boro: String?
    let overview_paragraph: String?
    let ell_programs: String?
    let neighborhood: String?
    let building_code: String?
    let location: String?
    let phone_number: String?
    let fax_number: String?
    let school_email: String?
    let website: String?
    let subway: String?
    let extracurricular_activities: String?
    let primary_address_line_1: String?
    let city: String?
    let id = UUID()
    
}
